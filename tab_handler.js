'use strict';

const tabs = {};
let current_tab_id = 0;

class TabInfo {
    safe = 0;
    unsafe = 0;
    unknown = 0;
    blocked = 0;
    current_url = '';
}

document.addEventListener('DOMContentLoaded', () => {
    load_settings();

    // Initialize active tab
    browser_api.tabs.query({ currentWindow: true, active: true }, active_tab => {
        current_tab_id = active_tab[0].id;
    });

    // Initialize tab data with zeros
    browser_api.tabs.query({}, init_tabs => {
        for (const tab of init_tabs) {
            tabs[tab.id] = new TabInfo();
        }
    });

    // Update the icon for the active tab
    schedule_icon_update();
});

let tab_update_timeout = false;

// Only update the icon at most 10 times per second
function schedule_icon_update() {
    if (tab_update_timeout === false) {
        tab_update_timeout = true;
        setTimeout(() => {
            update_icon();
            tab_update_timeout = false;
        }, 100);
    }
}

function change_tab_handler(tab_info) {
    current_tab_id = tab_info.tabId;
    // Make sure the tab is initialized
    if (tabs[current_tab_id] === undefined) {
        tabs[current_tab_id] = new TabInfo();
    }
    update_icon();
}

// Setup the listener
browser_api.tabs.onActivated.addListener(change_tab_handler);

// ================================ //
//    ====  UTILITY METHODS  ====   //
// ================================ //
const COLORS = {
    primary: '#28965A',
    secondary: '#FBB13C',
    grey: '#888d97',
    red: '#C84630'
};

function draw_arc(ctx, color, start, end, radius = 30) {
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.moveTo(32, 32);
    ctx.arc(32, 32, radius, start, end);
    ctx.fill();
}

function background_arc(ctx) {
    draw_arc(ctx, 'white', 0, 2 * Math.PI, 32);
}

function draw_question_mark(ctx) {
    background_arc(ctx);

    ctx.fillStyle = 'black';
    ctx.font = '44px sans-serif';
    ctx.textAlign = 'center';
    ctx.fillText('?', 32, 48);
}

function update_icon() {
    const tab = tabs[current_tab_id];
    const canvas = document.createElement('canvas');
    canvas.width = 64;
    canvas.height = 64;

    const ctx = canvas.getContext('2d');

    // Is data available?
    if (tab === undefined) {
        draw_question_mark(ctx);
    } else {
        // Is data valid?
        if (tab.safe === 0
            && tab.unsafe === 0
            && tab.unknown === 0
            && tab.blocked === 0) {
            draw_question_mark(ctx);
        } else {
            // Is everything safe?
            if (tab.unsafe + tab.unknown + tab.blocked === 0) {
                browser_api.browserAction.setIcon({
                    path: {
                        128: 'icons/logo_128.png'
                    }
                });
                return;
            }

            background_arc(ctx);
            const sum = tab.safe + tab.unsafe + tab.unknown + tab.blocked;
            // Start pie chart at 12 o'clock
            let angle = - Math.PI / 2;

            if (tab.safe !== 0) {
                const new_angle = tab.safe / sum * 2 * Math.PI;
                draw_arc(ctx, COLORS.primary, angle, angle + new_angle);
                angle += new_angle;
            }

            if (tab.unknown !== 0) {
                const new_angle = tab.unknown / sum * 2 * Math.PI;
                draw_arc(ctx, COLORS.grey, angle, angle + new_angle);
                angle += new_angle;
            }

            if (tab.blocked !== 0) {
                const new_angle = tab.blocked / sum * 2 * Math.PI;
                draw_arc(ctx, COLORS.secondary, angle, angle + new_angle);
                angle += new_angle;
            }

            if (tab.unsafe !== 0) {
                const new_angle = tab.unsafe / sum * 2 * Math.PI;
                draw_arc(ctx, COLORS.red, angle, angle + new_angle);
            }
        }
    }

    const data = ctx.getImageData(0, 0, 64, 64);
    browser_api.browserAction.setIcon({
        imageData: {
            64: data
        }
    });
}