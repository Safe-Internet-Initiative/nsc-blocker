# NSC Blocker

**The future of youth protection that respects your privacy!**

## About

This extension implements blocking and statistics regarding the ["NSC" header](https://safe-internet.org/).

It gives you the opportunity to try out the [new specification](https://safe-internet.org/specification) yourself and to analyze whether your own websites implements the specification correctly.

Please see our [website](https://safe-internet.org/web-extension) for more information.

## Motivation

Developing a new specification that changes how we use the internet is a difficult tasks.
Even thought the underlying concept is rather simple, every potential use-case should be covered and described in the specification.

To detect and address shortcomings in the specification, it's crucial to test the specification in real world scenarios.