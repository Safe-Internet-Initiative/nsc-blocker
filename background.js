'use strict';

function filter_headers(e) {
    const tab_id = e.tabId;
    const headers = e.responseHeaders;

    if (tabs[tab_id] === undefined) {
        // Make sure the tab is initialized
        tabs[tab_id] = new TabInfo();
    }

    if (e.type === 'main_frame') {
        // A new top-level document was loaded, reset the request stats 
        tabs[tab_id] = new TabInfo();
        tabs[tab_id].current_tab_url = e.url;
    }

    const tab = tabs[tab_id];

    // Do not count stats for responses from a stale tab
    let stat_increment;

    if (e.documentUrl) {
        const count_stats = e.documentUrl === tab.current_tab_url;
        stat_increment = count_stats ? 1 : 0;
    } else {
        stat_increment = e.type !== 'ping';
    }

    const idx = headers.findIndex(h => h.name.toLowerCase() == 'nsc');
    let block_response = false;
    let no_cache = false;

    if (idx !== -1) {
        // NSC header was found.
        const value = headers[idx].value;
        if (value === '0' || value === 'none') {
            tab.safe += stat_increment;
        } else {
            block_response = block_unsafe;
            if (!block_response) {
                tab.unsafe += stat_increment;
            }
            no_cache = true;
        }
    } else {
        block_response = block_unknown;
        if (!block_response) {
            tab.unknown += stat_increment;
        }
        // Cache unknown content for now to prevent
        // performance loss on most websites
        // no_cache = true;
    }

    schedule_icon_update();

    if (block_response) {
        tab.blocked += stat_increment;
        return {
            cancel: true,
        };
    } else if (no_cache) {
        // Don't cache unsafe content
        const headers = e.responseHeaders;
        const cc_idx = headers.findIndex((h) => h.name.toLowerCase() === 'cache-control')
        if (cc_idx !== -1) {
            // Modify existing header
            headers[cc_idx].value = 'no-store';
        } else {
            // Add new Cache-Control header
            headers.push({
                name: 'Cache-control',
                value: 'no-store',
            });
        }
        return {
            responseHeaders: headers,
        }
    } else {
        return {
            cancel: false,
        };
    }
}

function send_popup_data(request, _sender, sendResponse) {
    if (request.type === 'INFO') {
        sendResponse({
            response: {
                tab: tabs[current_tab_id],
                block_unsafe: block_unsafe,
                block_unknown: block_unknown,
            }
        });
    } else if (request.type === 'SET_BLOCK_UNSAFE') {
        block_unsafe = request.value;
        save_settings();
    } else if (request.type === 'SET_BLOCK_UNKNOWN') {
        block_unknown = request.value;
        save_settings();
    }
}

// Setup listeners
browser_api.runtime.onMessage.addListener(send_popup_data);
browser_api.webRequest.onHeadersReceived.addListener(
    filter_headers,
    {
        urls: ['<all_urls>']
    },
    ['blocking', 'responseHeaders']
);
