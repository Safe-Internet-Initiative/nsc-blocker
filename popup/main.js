'use strict';

// Cross-browser API compatabiltity 
var browser_api;
try {
    if (browser === undefined) {
        browser_api = chrome;
    } else {
        browser_api = browser;
    }
} catch (e) {
    // Catch reference error
    browser_api = chrome;
}

const byId = (id) => document.getElementById(id);

window.addEventListener('load', () => {
    const block_unsafe = byId('block-unsafe');
    const block_unknown = byId('block-unknown');

    block_unsafe.addEventListener("change", (ev) => {
        browser_api.runtime.sendMessage({
            type: 'SET_BLOCK_UNSAFE',
            value: ev.currentTarget.checked,
        });
    });

    block_unknown.addEventListener('change', (ev) => {
        browser_api.runtime.sendMessage({
            type: 'SET_BLOCK_UNKNOWN',
            value: ev.currentTarget.checked,
        });
    });

    // Find out when to popup is opened
    const observer = new IntersectionObserver(function (entries) {
        if (entries[0].isIntersecting === true) {
            update_info();
        }
    }, { threshold: [0] });

    const html = document.querySelector('html');
    observer.observe(html);
});

function update_info(elem, ev) {
    const row = {
        safe: byId('safe-row'),
        unknown: byId('unknown-row'),
        blocked: byId('blocked-row'),
        unsafe: byId('unsafe-row'),
    };

    const content = {
        safe: byId('safe-content'),
        unknown: byId('unknown-content'),
        blocked: byId('blocked-content'),
        unsafe: byId('unsafe-content'),
    };

    const chart = {
        background: byId('background-pie'),
        logo: byId('logo-chart'),
        safe: byId('safe-chart'),
        unsafe: byId('unsafe-chart'),
        unknown: byId('unknown-chart'),
        blocked: byId('blocked-chart'),
    };

    const request_count = byId('request-count');
    const question_mark = byId('question-mark');

    const block_unsafe = byId('block-unsafe');
    const block_unknown = byId('block-unknown');

    browser_api.runtime.sendMessage({
        type: 'INFO',
    }, info => {
        // Update buttons
        block_unsafe.checked = info.response.block_unsafe;
        block_unknown.checked = info.response.block_unknown;

        // Update chart
        const tab = info.response.tab;
        const sum = tab.safe + tab.unsafe + tab.unknown + tab.blocked;

        if (isNaN(sum) || sum === undefined || sum === 0) {
            // No valid data
            content.safe.innerText =
                content.unknown.innerText =
                content.blocked.innerText =
                content.unsafe.innerText =
                request_count.innerText = 'Na';

            [
                chart.background,
                chart.logo,
                chart.safe,
                chart.unsafe,
                chart.blocked
            ].forEach(elem => elem.classList.add("is-hidden"));

            chart.unknown.style.clipPath = "";
            [
                chart.unknown,
                question_mark,
                row.safe,
                row.unknown,
                row.blocked,
                row.unsafe
            ].forEach(elem => elem.classList.remove("is-hidden"));
        } else {
            // Valid data
            request_count.innerText = `${sum} request${sum > 1 ? 's' : ''}`;

            const percent = {
                safe: tab.safe * 100 / sum,
                unsafe: tab.unsafe * 100 / sum,
                unknown: tab.unknown * 100 / sum,
                blocked: tab.blocked * 100 / sum,
            };

            content.safe.innerText = Math.round(percent.safe) + '%';
            content.unsafe.innerText = Math.round(percent.unsafe) + '%';
            content.blocked.innerText = Math.round(percent.blocked) + '%';
            content.unknown.innerText = Math.round(percent.unknown) + '%';

            if (sum === tab.safe) {
                // Everything is safe!
                chart.logo.classList.remove("is-hidden");

                [
                    chart.background,
                    chart.safe,
                    chart.unsafe,
                    chart.unknown,
                    chart.blocked,
                    row.unknown,
                    row.blocked,
                    row.unsafe
                ].forEach(elem => elem.classList.add("is-hidden"));
            } else {
                // Mixed safety
                chart.logo.classList.add("is-hidden");
                chart.background.classList.remove("is-hidden");

                const break_points = {
                    start: 0,
                    safe: percent.safe,
                    unknown: percent.safe + percent.unknown,
                    blocked: percent.safe + percent.unknown + percent.blocked,
                    end: 100,
                };

                if (tab.safe === 0) {
                    row.safe.classList.add("is-hidden");
                    chart.safe.classList.add("is-hidden");
                } else {
                    row.safe.classList.remove("is-hidden");
                    chart.safe.classList.remove("is-hidden");
                    chart.safe.style.clipPath = clip_per_percent(
                        break_points.start, break_points.safe);
                }

                if (tab.unknown === 0) {
                    row.unknown.classList.add("is-hidden");
                    chart.unknown.classList.add("is-hidden");
                } else {
                    row.unknown.classList.remove("is-hidden");
                    chart.unknown.classList.remove("is-hidden");
                    chart.unknown.style.clipPath = clip_per_percent(
                        break_points.safe, break_points.unknown);
                }

                if (tab.blocked === 0) {
                    row.blocked.classList.add("is-hidden");
                    chart.blocked.classList.add("is-hidden");
                } else {
                    row.blocked.classList.remove("is-hidden");
                    chart.blocked.classList.remove("is-hidden");
                    chart.blocked.style.clipPath = clip_per_percent(
                        break_points.unknown, break_points.blocked);
                }

                if (tab.unsafe === 0) {
                    row.unsafe.classList.add("is-hidden");
                    chart.unsafe.classList.add("is-hidden");
                } else {
                    row.unsafe.classList.remove("is-hidden");
                    chart.unsafe.classList.remove("is-hidden");
                    chart.unsafe.style.clipPath = clip_per_percent(
                        break_points.blocked, break_points.end);
                }


                question_mark.classList.add("is-hidden");
            }
        }

    });
}

function clip_per_percent(start, end) {
    let clip = "polygon(50% 50%, ";

    const start_point = percent_rect_point(start);
    clip = clip + `${start_point.x}% ${start_point.y}%, `;

    if (start < 12.5) {
        clip = clip + "100% 0, ";
    }
    if (start < 25 && end > 12.5) {
        clip = clip + "100% 50%, ";
    }
    if (start < 37.5 && end > 25) {
        clip = clip + "100% 100%, ";
    }
    if (start < 50 && end > 37.5) {
        clip = clip + "50% 100%, ";
    }
    if (start < 62.5 && end > 50) {
        clip = clip + "0 100%, ";
    }
    if (start < 75 && end > 62.5) {
        clip = clip + "0 50%, ";
    }
    if (start < 87.5 && end > 75) {
        clip = clip + "0 0, ";
    }

    const { x, y } = percent_rect_point(end);

    return clip + `${x}% ${y}%)`;
}

function percent_rect_point(percent) {
    let equiv_percent = percent % 25;
    if (equiv_percent > 12.5) {
        equiv_percent = 25 - equiv_percent;
    }

    const c = Math.sin(equiv_percent * 2 * Math.PI / 100) * 50;

    let x, y;
    if (percent < 12.5) {
        x = 50 + c;
        y = 0;
    } else if (percent < 25) {
        x = 100;
        y = 50 - c;
    } else if (percent < 37.5) {
        x = 100;
        y = 50 + c;
    } else if (percent < 50) {
        x = 50 + c;
        y = 100;
    } else if (percent < 62.5) {
        x = 50 - c;
        y = 100;
    } else if (percent < 75) {
        x = 0;
        y = 50 + c;
    } else if (percent < 87.5) {
        x = 0;
        y = 50 - c;
    } else {
        x = 50 - c;
        y = 0;
    }

    return {
        x: x,
        y: y,
    };
}