'use strict';

// Cross-browser API compatibility 
var browser_api;
try {
    if (browser === undefined) {
        browser_api = chrome;
    } else {
        browser_api = browser;
    }
} catch (e) {
    // Catch reference error
    browser_api = chrome;
}

// Default values
let block_unsafe = true;
let block_unknown = false;

function load_settings() {
    browser_api.storage.sync.get('settings', result => {
        if (result.block_unsafe !== undefined) {
            block_unsafe = result.block_unsafe;
        };
        if (result.block_unknown !== undefined) {
            block_unknown = result.block_unknown;
        }
    });
}

function save_settings() {
    browser_api.storage.sync.set({
        settings: {
            block_unsafe: block_unsafe,
            block_unknown: block_unknown,
        },
    });
}